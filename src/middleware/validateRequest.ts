import { NextFunction, Request, Response } from "express";
import log from "../logger";
import { AnySchema } from "yup";

const validate =
  (schema: AnySchema) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      await schema.validate(
        {
          body: req.body,
          query: req.query,
          params: req.params,
        },
        { abortEarly: false }
      );
      return next();
    } catch (e) {
      log.error(e);
      return res.status(400).json({ succes: false, error: e.errors });
    }
  };
export default validate;
