import { Request, Response } from "express";
import log from "../logger";
import {
  createAccessToken,
  createSession,
  updateSession,
} from "../service/session.service";
import { validatePassword } from "../service/user.service";
import { sign } from "../utils/jwt.utils";
import config from "config";
import { get } from "lodash";

export const createUserSessionHandler = async (req: Request, res: Response) => {
  try {
    let user = await validatePassword(req.body);
    if (!user) return res.status(401).send("Invalid username or password");

    const session = await createSession(user._id, req.get("user-agent") || "");
    const accessToken = createAccessToken({ user, session });
    const refreshToken = sign(session, {
      expiresIn: config.get("refreshTokenTtl"),
    });
    return res.send({ accessToken, refreshToken });
  } catch (e) {
    log.error(e);
    res.status(409).send(e.message);
  }
};

export const invalidateUserSessionHandler = async (
  req: Request,
  res: Response
) => {
  const sessionId = get(req, "user.session");

  await updateSession({ _id: sessionId }, { valid: false });
  return res.sendStatus(200);
};
