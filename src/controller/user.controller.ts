import { Request, Response } from "express";
import { omit } from "lodash";
import log from "../logger";
import { createUser } from "../service/user.service";

export const createUserHandler = async (req: Request, res: Response) => {
  try {
    let user = await createUser(req.body);
    return res.send(omit(user.toJSON(), "password"));
  } catch (e) {
    log.error(e);
    res.status(409).send(e.message);
  }
};
