import mongoose from "mongoose";
import config from "config";
import log from "../logger";

const connect = () => {
  const dburi = config.get("dbUri") as string;

  return mongoose
    .connect(dburi, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      log.info("Database Connected");
    })
    .catch((error) => {
      log.error("Db error", error);
      process.exit(1);
    });
};
export default connect;
