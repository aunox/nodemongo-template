import { omit } from "lodash";
import { DocumentDefinition, FilterQuery } from "mongoose";
import User, { UserDocumnet } from "../model/user.model";

export const createUser = async (input: DocumentDefinition<UserDocumnet>) => {
  try {
    return await User.create(input);
  } catch (error) {
    throw new Error(error);
  }
};

export const findUser = async (query: FilterQuery<UserDocumnet>) => {
  return await User.findOne(query).lean();
};

export const validatePassword = async ({
  email,
  password,
}: {
  email: UserDocumnet["email"];
  password: string;
}) => {
  const user = await User.findOne({ email });
  if (!user) return false;

  const isValid = await user.comparePassword(password);
  if (!isValid) return false;

  return omit(user.toJSON(), "password");
};
