import { object, ref, string } from "yup";

export const createUserSchema = object({
  body: object({
    name: string().required("name is required"),
    password: string()
      .required("password is required")
      .min(6, "password is too short - should be 6 character minimum"),
    passwordConfrimation: string().oneOf(
      [ref("password"), null],
      "password do not match"
    ),
    email: string()
      .email("must be a valid email")
      .required("email is required"),
  }),
});

export const createUserSessionSchema = object({
  body: object({
    email: string()
      .email("must be a valid email")
      .required("email is required"),
    password: string()
      .required("password is required")
      .min(6, "password is too short - should be 6 character minimum"),
  }),
});
