import { Express, Request, Response } from "express";
import {
  createUserSessionHandler,
  invalidateUserSessionHandler,
} from "./controller/session.controller";
import { createUserHandler } from "./controller/user.controller";
import { validateRequest, requiresUser } from "./middleware";
import {
  createUserSchema,
  createUserSessionSchema,
} from "./schema/user.schema";

export default function (app: Express) {
  app.get("/check", (req: Request, res: Response) => {
    res.status(200).json({ success: true });
  });

  app.post("/api/users", validateRequest(createUserSchema), createUserHandler);

  //login route
  app.post(
    "/api/sessions",
    validateRequest(createUserSessionSchema),
    createUserSessionHandler
  );

  //logout route
  app.delete("/api/sessions", requiresUser, invalidateUserSessionHandler);
}
